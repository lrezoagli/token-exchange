var Utils = artifacts.require('Utils.sol');
var Owned = artifacts.require('Owned.sol');
var ERC20Token = artifacts.require('ERC20Token.sol');
var Exchange = artifacts.require('Exchange.sol');

module.exports = function (deployer) {
    deployer.deploy(Utils);
    deployer.deploy(Owned);
    deployer.deploy(ERC20Token);
    deployer.deploy(Exchange);
};
