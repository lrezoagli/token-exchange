pragma solidity ^0.4.19;

contract IERC20Token {
    // these functions aren't abstract since the compiler emits automatically generated getter functions as external
    function name() public view returns (string);

    function symbol() public view returns (string);

    function decimals() public view returns (uint8);

    function totalSupply() public view returns (uint256);

    function balanceOf(address _owner) public view returns (uint256);

    function allowance(address _owner, address _spender) public view returns (uint256);

    function transfer(address _to, uint256 _value) public returns (bool success);

    function transferFrom(address _from, address _to, uint256 _value) public returns (bool success);

    function approve(address _spender, uint256 _value) public returns (bool success);
}

contract IOwned {

    function owner() public view returns (address);

    function transferOwnership(address _newOwner) public;

}

contract Owned is IOwned {

    address public owner;
    address public newOwner;

    event OwnerUpdate(address indexed _prevOwner, address indexed _newOwner);

    function Owned() public {
        owner = msg.sender;
    }

    function owner() public view returns(address){
        return owner;
    }

    modifier ownerOnly {
        assert(msg.sender == owner);
        _;
    }

    function transferOwnership(address _newOwner) public ownerOnly {
        require(_newOwner != owner);
        newOwner = _newOwner;
    }
}

contract Utils {

    function Utils() public {
    }

    modifier greaterThanZero(uint256 _amount) {
        require(_amount > 0);
        _;
    }

    modifier validAddress(address _address) {
        require(_address != address(0));
        _;
    }

    modifier notThis(address _address) {
        require(_address != address(this));
        _;
    }
    function safeAdd(uint256 _x, uint256 _y) internal pure returns (uint256) {
        uint256 z = _x + _y;
        assert(z >= _x);
        return z;
    }

    function safeSub(uint256 _x, uint256 _y) internal pure returns (uint256) {
        assert(_x >= _y);
        return _x - _y;
    }

    function safeMul(uint256 _x, uint256 _y) internal pure returns (uint256) {
        uint256 z = _x * _y;
        assert(_x == 0 || z / _x == _y);
        return z;
    }
}

contract ERC20Token is IERC20Token, Utils {
    string public standard = 'Token ERC20 ';
    string public name = 'RCN Curso';
    string public symbol = 'RCNC';
    uint8 public decimals = 18;
    uint256 public totalSupply = 1000000000;
    mapping(address => uint256) public balanceOf;
    mapping(address => mapping(address => uint256)) public allowance;

    event Transfer(address indexed _from, address indexed _to, uint256 _value);
    event Approval(address indexed _owner, address indexed _spender, uint256 _value);


    function ERC20Token() public {
        balanceOf[msg.sender] = totalSupply;
        Transfer(address(0), msg.sender, totalSupply);
    }

    function transfer(address _to, uint256 _value) public validAddress(_to) returns (bool success) {
        balanceOf[msg.sender] = safeSub(balanceOf[msg.sender], _value);
        balanceOf[_to] = safeAdd(balanceOf[_to], _value);
        Transfer(msg.sender, _to, _value);
        return true;
    }


    function transferFrom(address _from, address _to, uint256 _value) public validAddress(_from) validAddress(_to) returns (bool success){
        allowance[_from][msg.sender] = safeSub(allowance[_from][msg.sender], _value);
        balanceOf[_from] = safeSub(balanceOf[_from], _value);
        balanceOf[_to] = safeAdd(balanceOf[_to], _value);
        Transfer(_from, _to, _value);
        return true;
    }

    function approve(address _spender, uint256 _value) public validAddress(_spender) returns (bool success){
        allowance[msg.sender][_spender] = _value;
        Approval(msg.sender, _spender, _value);
        return true;
    }

    function balanceOf(address _owner) view returns (uint256 balance) {
        return balanceOf[_owner];
    }

    function allowance(address _owner, address _spender) view returns (uint256 remaining) {
      return allowance[_owner][_spender];
    }

    function totalSupply() view returns(uint256){
        return totalSupply;
    }

     function name() view returns(string){
         return name;
     }

     function symbol() view returns(string){
         return symbol;
     }

    function decimals() view returns (uint8){
        return decimals;
    }
}

contract Exchange is Utils, Owned {

    struct Order {
        address user;
        uint price;
        uint size;
        bool side;
    }

    mapping(address => mapping(address => uint)) public tokens;
    mapping(bytes32 => Order[]) public orders;

    event NewOrder(address _tokenAddress, address _sender, uint _size, uint _priceETH, bool _side );
    event NewTrade(address _tokenAddress, address _buyAddress, address _sellAddress, uint _size, uint _priceETH);
    event DepositETH(address _address, uint _amount);
    event DepositToken(address _tokenAddress,  address _address, uint _amount);
    event Withdraw(address token, address user, uint amount, uint balance);


    function deposit() payable {
        tokens[0][msg.sender] = safeAdd(tokens[0][msg.sender], msg.value);
        DepositETH(msg.sender, msg.value);
    }

    function withdraw(uint amount) {
        require(tokens[0][msg.sender] <= amount);
        tokens[0][msg.sender] = safeSub(tokens[0][msg.sender], amount);
        Withdraw(0, msg.sender, amount, tokens[0][msg.sender]);
    }

    function depositToken(address _token, uint _amount) public  returns (bool) {
        require(IERC20Token(_token).transferFrom(msg.sender, address(this), _amount));
        tokens[_token][msg.sender] = safeAdd(tokens[_token][msg.sender], _amount);
        DepositToken(_token,msg.sender, _amount);
        return true;
    }

    function withdrawToken(address token, uint amount) {
        require(tokens[token][msg.sender] <= amount);
        tokens[token][msg.sender] = safeSub(tokens[token][msg.sender], amount);
        Withdraw(token, msg.sender, amount, tokens[token][msg.sender]);
    }

    function newOrder(address _tokenAddress, uint _size, uint _priceETH, bool _side) public returns (bool){
        //chequear que tenga los token o los ETH
        bytes32 hash = sha256(_tokenAddress, _size, _priceETH);
        Order memory order = Order(msg.sender, _priceETH, _size, _side);
        if (!findTrade(_tokenAddress, hash, order)) {
            orders[hash].push(order);
        }
        NewOrder(_tokenAddress, msg.sender, _size,  _priceETH, _side );
        return true;
    }

    function findTrade(address _token, bytes32 _hash, Order _order) private returns (bool){

        Order[] ordersFind = orders[_hash];
        uint256 countOrder = ordersFind.length;
        for (uint i = 0; i < countOrder; i++) {
            require(ordersFind[i].side != _order.side);
            if (_order.side) {
                sendToken(_token, _order, ordersFind[i]);
                NewTrade(_token, _order.user, ordersFind[i].user, _order.size, _order.price);
            } else {
                sendToken(_token, ordersFind[i], _order);
                NewTrade(_token, ordersFind[i].user, _order.user, _order.size, _order.price);
            }
            delete ordersFind[i];
            orders[_hash] = ordersFind;
            return true;
        }
        return false;
    }

    function sendToken(address _token, Order _orderBuy, Order _orderSell) private returns (bool){
        //enviar fondos
        tokens[0][_orderSell.user] = safeAdd(tokens[0][_orderSell.user], safeMul(_orderBuy.size, _orderBuy.price));
        tokens[_token][_orderBuy.user] = safeAdd(tokens[_token][_orderBuy.user], safeMul(_orderSell.size, _orderSell.price));

        //restart los fondos
        tokens[0][_orderBuy.user] = safeSub(tokens[0][_orderBuy.user], safeMul(_orderBuy.size, _orderBuy.price));
        tokens[_token][_orderSell.user] = safeSub(tokens[_token][_orderSell.user], safeMul(_orderSell.size, _orderSell.price));
        return true;
    }


}