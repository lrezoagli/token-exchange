pragma solidity ^0.4.18;

import './../interfaces/IOwned.sol';


contract Owned is IOwned {

    address public owner;
    address public newOwner;

    event OwnerUpdate(address indexed _prevOwner, address indexed _newOwner);

    function Owned() public {
        owner = msg.sender;
    }

    function owner() public view returns(address){
        return owner;
    }

    modifier ownerOnly {
        assert(msg.sender == owner);
        _;
    }

    function transferOwnership(address _newOwner) public ownerOnly {
        require(_newOwner != owner);
        newOwner = _newOwner;
    }
}