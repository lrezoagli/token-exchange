pragma solidity ^0.4.19;

/*
    Owned contract interface
*/
contract IOwned {

    function owner() public view returns (address);

    function transferOwnership(address _newOwner) public;

}