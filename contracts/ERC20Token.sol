pragma solidity ^0.4.19;

import './utils/Utils.sol';
import './interfaces/IERC20Token.sol';

contract ERC20Token is IERC20Token, Utils {
    string public standard = 'Token ERC20 ';
    string public name = 'RCN Curso';
    string public symbol = 'RCNC';
    uint8 public decimals = 18;
    uint256 public totalSupply = 1000000000;
    mapping(address => uint256) public balanceOf;
    mapping(address => mapping(address => uint256)) public allowance;

    event Transfer(address indexed _from, address indexed _to, uint256 _value);
    event Approval(address indexed _owner, address indexed _spender, uint256 _value);


    function ERC20Token() public {
        balanceOf[msg.sender] = totalSupply;
        Transfer(address(0), msg.sender, totalSupply);
    }

    function transfer(address _to, uint256 _value) public validAddress(_to) returns (bool success) {
        balanceOf[msg.sender] = safeSub(balanceOf[msg.sender], _value);
        balanceOf[_to] = safeAdd(balanceOf[_to], _value);
        Transfer(msg.sender, _to, _value);
        return true;
    }


    function transferFrom(address _from, address _to, uint256 _value) public validAddress(_from) validAddress(_to) returns (bool success){
        allowance[_from][msg.sender] = safeSub(allowance[_from][msg.sender], _value);
        balanceOf[_from] = safeSub(balanceOf[_from], _value);
        balanceOf[_to] = safeAdd(balanceOf[_to], _value);
        Transfer(_from, _to, _value);
        return true;
    }

    function approve(address _spender, uint256 _value) public validAddress(_spender) returns (bool success){
        allowance[msg.sender][_spender] = _value;
        Approval(msg.sender, _spender, _value);
        return true;
    }

    function balanceOf(address _owner) view returns (uint256 balance) {
        return balanceOf[_owner];
    }

    function allowance(address _owner, address _spender) view returns (uint256 remaining) {
      return allowance[_owner][_spender];
    }

    function totalSupply() view returns(uint256){
        return totalSupply;
    }

     function name() view returns(string){
         return name;
     }

     function symbol() view returns(string){
         return symbol;
     }

    function decimals() view returns (uint8){
        return decimals;
    }
}