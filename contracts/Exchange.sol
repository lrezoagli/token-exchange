pragma solidity ^0.4.19;

import './utils/Utils.sol';
import './utils/Owned.sol';
import './interfaces/IERC20Token.sol';

contract Exchange is Utils, Owned {

    struct Order {
        address user;
        uint price;
        uint size;
        bool side;
    }

    mapping(address => mapping(address => uint)) public tokens;
    mapping(bytes32 => Order[]) public orders;

    event NewOrder(address _tokenAddress, address _sender, uint _size, uint _priceETH, bool _side);
    event NewTrade(address _tokenAddress, address _buyAddress, address _sellAddress, uint _size, uint _priceETH);
    event DepositETH(address _address, uint _amount);
    event DepositToken(address _tokenAddress, address _address, uint _amount);
    event Withdraw(address token, address user, uint amount, uint balance);


    function deposit() payable {
        tokens[0][msg.sender] = safeAdd(tokens[0][msg.sender], msg.value);
        DepositETH(msg.sender, msg.value);
    }

    function withdraw(uint amount) {
        require(tokens[0][msg.sender] <= amount);
        tokens[0][msg.sender] = safeSub(tokens[0][msg.sender], amount);
        Withdraw(0, msg.sender, amount, tokens[0][msg.sender]);
    }

    function depositToken(address _token, uint _amount) public {
        require(IERC20Token(_token).transferFrom(msg.sender, address(this), _amount));
        tokens[_token][msg.sender] = safeAdd(tokens[_token][msg.sender], _amount);
        DepositToken(_token, msg.sender, _amount);
    }

    function withdrawToken(address token, uint amount) {
        require(tokens[token][msg.sender] <= amount);
        tokens[token][msg.sender] = safeSub(tokens[token][msg.sender], amount);
        Withdraw(token, msg.sender, amount, tokens[token][msg.sender]);
    }

    function newOrder(address _tokenAddress, uint _size, uint _priceETH, bool _side) public returns (bool){
        //chequear que tenga los token o los ETH
        bytes32 hash = sha256(_tokenAddress, _size, _priceETH);
        Order memory order = Order(msg.sender, _priceETH, _size, _side);
        if (!findTrade(_tokenAddress, hash, order)) {
            orders[hash].push(order);
        }
        NewOrder(_tokenAddress, msg.sender, _size, _priceETH, _side);
        return true;
    }

    function findTrade(address _token, bytes32 _hash, Order _order) private returns (bool){

        Order[] ordersFind = orders[_hash];
        uint256 countOrder = ordersFind.length;
        for (uint i = 0; i < countOrder; i++) {
            require(ordersFind[i].side != _order.side);
            if (_order.side) {
                sendToken(_token, _order, ordersFind[i]);
                NewTrade(_token, _order.user, ordersFind[i].user, _order.size, _order.price);
            } else {
                sendToken(_token, ordersFind[i], _order);
                NewTrade(_token, ordersFind[i].user, _order.user, _order.size, _order.price);
            }

            delete ordersFind[i];
            orders[_hash] = ordersFind;
            return true;
        }
        return false;
    }

    function sendToken(address _token, Order _orderBuy, Order _orderSell) private returns (bool){
        //enviar fondos
        tokens[0][_orderSell.user] = safeAdd(tokens[0][_orderSell.user], safeMul(_orderBuy.size, _orderBuy.price));
        tokens[_token][_orderBuy.user] = safeAdd(tokens[_token][_orderBuy.user], safeMul(_orderSell.size, _orderSell.price));

        //restart los fondos
        tokens[0][_orderBuy.user] = safeSub(tokens[0][_orderBuy.user], safeMul(_orderBuy.size, _orderBuy.price));
        tokens[_token][_orderSell.user] = safeSub(tokens[_token][_orderSell.user], safeMul(_orderSell.size, _orderSell.price));
        return true;
    }


}